#!/bin/sh
cp config.toml $HOME/.local/share/io.parity.ethereum/. && \
parity account import keys/* --chain dev
