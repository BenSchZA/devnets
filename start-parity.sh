#!/bin/bash
mkdir -p /root/.local/share/io.parity.ethereum && \
cp /app/config.toml /root/.local/share/io.parity.ethereum/config.toml && \
parity account import /app/keys/* --chain /app/chain.conf && \
parity --config dev --jsonrpc-apis "all" --geth --chain /app/chain.conf \
    --jsonrpc-interface 0.0.0.0 \
    --jsonrpc-port 8545 \
    --reseal-min-period 0 \
    --unlock=0xd9995bae12fee327256ffec1e3184d492bd94c31,0x87e0ed760fb316eeb94bd9cf23d1d2be87ace3d8,0xd4fa489eacc52ba59438993f37be9fcc20090e39,0x760bf27cd45036a6c486802d30b5d90cffbe31fe,0x56a32fff5e5a8b40d6a21538579fb8922df5258c,0xfec44e15328b7d1d8885a8226b0858964358f1d6,0xda8a06f1c910cab18ad187be1faa2b8606c2ec86,0x8199de05654e9afa5c081bce38f140082c9a7733,0x28bf45680ca598708e5cdacc1414fcac04a3f1ed,0xf0508f89e26bd6b00f66a9d467678c7ed16a3c5a \
    --password=/app/emptypasswordfile.secret
