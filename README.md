# Ethereum Development Chains

Research into alternative Ethereum development chains for testing smart contracts. Ganache is often outdated and has its issues, so here are a few alternatives.

## Current Viable Alternatives

The relevant repos have been added as submodules in this repo, with the following branches in active development:

* 0x Devnet: branch `linumlabs-development`
* Cliquebait: branch `master`

### 0x Project: Devnet

* Project: https://0x.org/
* Repository: https://github.com/0xProject/0x-monorepo/tree/development/packages/devnet

A pull request was merged to add `docker-compose` support, so simply run the following command to expose the service at port `8501`.

**NB:** The pre-configured accounts used for the other devnet alternatives were pulled from the 0x Project repo, so no adjustments need to be made.

* Command: `docker-compose up --build`

### FOAM Project: Cliquebait

* Project: https://www.foam.space/
* Repository: https://github.com/f-o-a-m/cliquebait

* Command: `docker run --rm -it -p 8501:8545 -v ./cliquebait/extra-accounts.json:/extra-accounts.json foamspace/cliquebait:latest`

**NB:** Replace `latest` with version defined in `cliquebait/build.sh` when using custom locally built image.

### Geth Devnet

* Documentation (hasn't been merged yet): https://github.com/ethereum/go-ethereum/issues/14704 && https://github.com/curvegrid/go-ethereum-wiki/pull/1/files

* Command: `geth --dev --rpcapi db,eth,net,web3,personal,debug --datadir dev-geth`

The command will create a directory called `dev-geth` in the current directory, here the data will be persisted for further runs.

### Parity Devnet

* Documentation: https://wiki.parity.io/Private-development-chain

**NB:** In case of corruption or changing Parity versions, it may sometimes be necessary to remove the devnet chaindata, located at `~/.local/share/io.parity.ethereum/...`.

* Command: `parity --config dev --jsonrpc-apis "all" --geth`

The command has a number of options for `--config`, in this case dev was used, which starts a devnet with instant seal enabled.

## Importing Keys

`parity account import keys/* --chain dev`

`geth account import privatekeys/*`

## Etherlime Devnet Deployer

A number of changes have been made to Etherlime, and submitted in a PR, in order to interface with Ganache alternatives.

A new Etherlime deployer class has been created, with an alternative set of test accounts pre-configured:

* Ganache accounts global variable: `accounts`
* Devnet accounts global variable: `devnetAccounts`

* Ganache deployer: `deployer = await new etherlime.EtherlimeGanacheDeployer(NodeSigner.secretKey);`
* Devnet deployer: `deployer = new etherlime.EtherlimeDevnetDeployer(NodeSigner.secretKey);`

* Ganache `ContractAt`: `basicLinearMarketInstance = await etherlime.ContractAt(BasicLinearMarket, marketAddress[0]);`
* Devnet `ContractAtDevnet`: `basicLinearMarketInstance = await etherlime.ContractAtDevnet(BasicLinearMarket, marketAddress[0]);`

## Open Issues/PRs

1. Etherlime ~ "Alternative to Ganache: Devnet deployer": https://github.com/LimeChain/etherlime/pull/154

## (WIP) Examples of Ganache specific issues

### https://github.com/0xProject/0x-monorepo/issues/1520

### Protea/Molecule token minting with PDai collateral

Minting of (Molecule/Protea) tokens for user, with PDai as the collateral, gets reverted. PDai approval seemingly failed when inspecting variables using Truffle transaction debugger, while assertions of earlier transaction success seem to have passed and proved otherwise. The same test performed on Remix JavaScript VM behaves as expected.

## Debugging and Local Development

### Steps Taken

1. Using `npm link`, linked local development version of Etherlime to experiment with different Solc compiler versions, Ganache versions, and Ethers.js versions - with guidance from Etherlime team on issue
2. Within Etherlime, created dev net interface classes, making necessary changes to template Ganache interface config
    * See https://github.com/LimeChain/etherlime/pull/154
3. Ensuring the genesis configuration makes logical sense, and when testing environments that they are all configured the same

### Using `npm link` to use development version of tools

A few adjustments were made to Etherlime as described above. These changes have not been merged yet. In the meantime we can use `npm link` to load a local version of the package with our changes.

1. Clone repository locally
2. Checkout the relevant branch
3. Run `npm link` in the package, for example Etherlime, root directory
4. Run `npm link etherlime` for example in the project directory

**OR**

Add/replace `"etherlime": "BenSchZA/etherlime#temp-feature",` in `package.json`

In the case of Etherlime, the open issue for the Devnet deployer needs to be cloned and checked out to be able to use the new feature.
