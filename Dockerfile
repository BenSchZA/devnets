FROM ubuntu:bionic

RUN apt-get update && apt-get install -y sudo curl

RUN mkdir -p /app/keys
WORKDIR /app

COPY keys/* /app/keys/
COPY chain.conf /app
COPY config.toml /app
COPY emptypasswordfile.secret /app
COPY install-parity.sh /app
COPY start-parity.sh /app

RUN ls /app

RUN /app/install-parity.sh

ENTRYPOINT ["/app/start-parity.sh"]
EXPOSE 8545